package main

import("fmt"
  "math")

func main(){
  var input int
  fmt.Print("What is your favorite integer? ")
  fmt.Scanf("%d", &input)

  switch{
  case input > -1:
    fmt.Printf("\t%d is a positive number\n", input)
  default:
    fmt.Printf("\t%d is a negitive number\n", input)
  }

  digits := math.Floor(math.Log10(float64(input)))+1

  switch{
  case input < 10:
    fmt.Printf("\tit is less than 10\n")
  default:
    fmt.Printf("\tit is greater than %d\n", int(math.Pow(10, digits)/10))
  }

  switch input % 2 == 0{
  case true:
    fmt.Printf("\tit is even\n")
  case false:
    fmt.Printf("\tit is odd\n")
  }

  switch(input){
  case 2:
    fmt.Printf("\tit is the atomic value of Helium\n")
  case 10:
    fmt.Printf("\tit is the atomic value of Neon\n")
  case 18:
    fmt.Printf("\tit is the atomic value of Argon\n")
  case 36:
    fmt.Printf("\tit is the atomic value of Krypton\n")
  case 54:
    fmt.Printf("\tit is the atomic value of Xenon\n")
  case 86:
    fmt.Printf("\tit is the atomic value of Radon\n")
  default:
    fmt.Printf("\tit is not an atomic value of a noble gas\n")
  }

  var charInput string
  var isUpper bool
  var isAlpha bool = false;

  fmt.Print("What is your favorite char? ")
  fmt.Scanf("%s", &charInput)

  if charInput[0] > 64 && charInput[0] < 91 {
    isAlpha = true;
    isUpper = true;
    fmt.Printf("\t%s is an uppercase letter\n", string(charInput[0]))
  } else if charInput[0] > 96 && charInput[0] < 123 {
    isAlpha = true;
    isUpper = false;
    fmt.Printf("\t%s is an lowercase letter\n", string(charInput[0]))
  } else if charInput[0] > 47 && charInput[0] < 58 {
    fmt.Printf("\t%s is a numerical digit\n", string(charInput[0]))
  } else {
    fmt.Printf("\t%s is an ASCII character\n", string(charInput[0]))
  }

  if isAlpha {
    switch charInput[0] {
    case 65, 69, 73, 79, 85, 97, 101, 105, 111, 117:
      fmt.Printf("\tit is a vowel\n")
    default:
      fmt.Printf("\tit is not a vowel\n")
    }
  }

  fmt.Printf("\tits ASCII value is %d\n", charInput[0])

  if isAlpha {
    var alphaValue uint8
    if isUpper {
      alphaValue = charInput[0] - 64;
    } else {
      alphaValue = charInput[0] - 96;
    }

    switch alphaValue {
    case 1, 21:
      fmt.Printf("\tit is the %dst letter of the alphabet\n", alphaValue)
    case 2, 22:
      fmt.Printf("\tit is the %dnd letter of the alphabet\n", alphaValue)
    case 3, 23:
      fmt.Printf("\tit is the %drd letter of the alphabet\n", alphaValue)
    default:
      fmt.Printf("\tit is the %dth letter of the alphabet\n", alphaValue)
    }
  }
}
