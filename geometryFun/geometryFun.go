package main

import("fmt"
  "math")

func main(){
  var input int
  fmt.Print("Please enter a whole number: ")
  fmt.Scanf("%d", &input)

  perimeter := input * 4
  squareArea := math.Pow(float64(input), 2)

  radius := float64(input) / 2
  circumference := math.Pi * float64(input)
  circleArea := math.Pi * math.Pow(radius, 2)

  fmt.Println("\nA square with side length of", input)
  fmt.Println("\thas a perimeter of", perimeter)
  fmt.Println("\thas an area of", squareArea)

  fmt.Println("\nA circle with a diameter of", input)
  fmt.Println("\thas a radius of", radius)
  fmt.Println("\thas a circumference of", circumference)
  fmt.Println("\thas an area of", circleArea)
}
