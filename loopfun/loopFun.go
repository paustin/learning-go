package main

import("fmt")

func main(){
  var height uint
  var width uint
  var total uint = 0

  fmt.Printf("Enter a box height(between 3 and 10): ")
  fmt.Scanf("%d", &height)

  for height < 3 || height > 10 {
    fmt.Printf("That number is out of bounds: Try again: ")
    fmt.Scanf("%d", &height)
  }

  fmt.Printf("Enter a box width (between %d and 20): ", height+1)
  fmt.Scanf("%d", &width)

  for width < height+1 || width > 20 {
    fmt.Printf("That number is out of bounds: Try again: ")
    fmt.Scanf("%d", &width)
  }

  fmt.Printf("\nThe integers from %d to %d are:\n\t", height, width)
  for i := height; i <= width; i++ {
    total = total + i
    fmt.Printf("%d ", i)
  }

  fmt.Printf("\nand the average of those numbers is %.2f\n", float64(total)/float64((width-height)+1))

  for i := uint(0); i < width; i++ {
    fmt.Printf("*")
  }

  for i := uint(0); i < height - 2; i++ {
    fmt.Printf("\n*")
    for j := uint(0); j < width - 2; j++ {
      fmt.Printf(" ")
    }
    fmt.Printf("*")
  }

  fmt.Printf("\n")

  for i := uint(0); i < width; i++ {
    fmt.Printf("*")
  }

  fmt.Printf("\n")
}
